jQuery(document).ready(function ($) {

  $(window).bind('orientationchange resize', function(e) {
    if (window.RT) clearTimeout(window.RT);
    window.RT = setTimeout(function() {
      this.location.reload(false); /* false to get page from cache */
    }, 100);
  });


  //Header Background OnScroll
  $(function() {
    $(window).on("scroll", function() {
      if($(window).scrollTop() > 5) {
        $("#logo").addClass("white--background");
        $(".topmenu").addClass("white--background");
        if ($(window).width() < 769) {
        $("header").addClass("white--background");  
          }
      } else {
            //remove the background property so it comes transparent again (defined in your css)
            $("#logo").removeClass("white--background");
            $(".topmenu").removeClass("white--background");
            if ($(window).width() < 769) {
        $("header").removeClass("white--background");  
          }
          }
        });
  });

  //Hover Effects

  $('.black-red')

  .on('mouseover', function(event) {
    $(this).find('.black-red-inner').css('color', '#ff4433');

  }).on('mouseout', function(event) {
    $(this).find('.black-red-inner').css('color', '#000');

  })
// Emulate Touch
.on('click', function(event) {
  $(this).find('.black-red-inner').css('color', '#ff4433');

});

$('.red-black')

.on('mouseover', function(event) {
  $(this).find('.red-black-inner').css('color', '#000');

}).on('mouseout', function(event) {
  $(this).find('.red-black-inner').css('color', '#ff4433');

})
// Emulate Touch
.on('click', function(event) {
  $(this).find('.red-black-inner').css('color', '#000');

});

$('.black-white')

.on('mouseover', function(event) {
  $(this).find('.black-white-inner').css('color', '#fff');

}).on('mouseout', function(event) {
  $(this).find('.black-white-inner').css('color', '#000');

})
// Emulate Touch
.on('click', function(event) {
  $(this).find('.black-white-inner').css('color', '#fff');

});


$('.white-red')

.on('mouseover', function(event) {
  $(this).find('.white-red-inner').css('color', '#ff4433');

}).on('mouseout', function(event) {
  $(this).find('.white-red-inner').css('color', '#fff');

})
// Emulate Touch
.on('click', function(event) {
  $(this).find('.white-red-inner').css('color', '#ff4433');

});

$('.white-black')

.on('mouseover', function(event) {
  $(this).find('.white-black-inner').css('color', '#000');

}).on('mouseout', function(event) {
  $(this).find('.white-black-inner').css('color', '#fff');

})
// Emulate Touch
.on('click', function(event) {
  $(this).find('.white-black-inner').css('color', '#000');

});

  //Beating Heart
  let state = $('#svg-heart').css('animation-play-state');

  function playAnimation(){
    if (state === "paused") {
      state = $('#svg-heart').css('animation-play-state', 'running');
    }
  };

  function resetAnimation () {
    if (state === "running") {
      state = $('#svg-heart').css('animation-play-state', 'paused');
    }
  };

  // Slider
  var slideCount = $('#slider ul li').length;
  var slideWidth = $('#slider ul li').width();
  var slideHeight = $('#slider ul li').height();
  var sliderUlWidth = slideCount * slideWidth;

  $('#slider').css({ width: slideWidth, height: slideHeight });

  $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

  $('#slider ul li:last-child').prependTo('#slider ul');

  function moveLeft() {
    $('#slider ul').animate({
      left: + slideWidth
    }, 600, function () {
      $('#slider ul li:last-child').prependTo('#slider ul');
      $('#slider ul').css('left', '');
    });
  };

  function moveRight() {
    $('#slider ul').animate({
      left: - slideWidth
    }, 600, function () {
      $('#slider ul li:first-child').appendTo('#slider ul');
      $('#slider ul').css('left', '');
    });
  };

  $('a.control_next').click(function () {
    moveLeft();
  });

  $('a.control_prev').click(function () {
    moveRight();
  });

  setInterval(function () {
    moveLeft();
    playAnimation();
  }, 4000, resetAnimation());

  
// typed.js
var typed = new Typed('#typed', {
  stringsElement: '#typed-strings',
  startDelay: 700,
  typeSpeed: 70,
  showCursor: true,
  cursorChar: '|',
  autoInsertCss: true,
  smartBackspace: false,
  backSpeed: 35,
  backDelay: 3500,
  loop: true
});

});